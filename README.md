# learning

学海无涯

## Getting Started

1. Communication: Sign up to [xuanyou.slack.com](https://xuanyou.slack.com/)
2. Say hello to everyone on #general!
3. Code Repository, CI: Create an account on [Gitlab](https://gitlab.com/users/sign_in#register-pane)
4. Cloud Infra: Create an account on [Amazon Web Services](https://portal.aws.amazon.com/billing/signup)

## Stage 1 - Serverless

1. Create a Lambda function - Follow the AWS Lambda [tutorial](https://aws.amazon.com/getting-started/tutorials/run-serverless-code/)
2. Attach a HTTPS endpoint to the Lambda function
3. Check the Lambda function is reachable from the HTTPS endpoint

More references:
- [freecodecamp: How to run your first AWS Lambda function in the cloud](https://www.freecodecamp.org/news/going-serverless-how-to-run-your-first-aws-lambda-function-in-the-cloud-d866a9b51536/)

## Stage 2 - Docker

1. Install docker & docker-compose
   - For Windows users, you should directly install VirtualBox/VMware Workstation and a Linux VM: it's faster
2. Install docker-compose
3. Create a new container based on a web server (node, nginx-php, etc)
4. Use dockerfile and docker-compose to copy changes to the webserver
5. Use docker-compose to create a Phoenix webservice
   - The definition of Phoenix: Whenever there's a configuration change or new code in the service, the docker container should be destroyed and rebuilt from scratch.
   - Hint - use the `build` option in docker-compose

## Stage 2.5 - CI/CD into docker

1. Commit your code to Gitlab
2. Install a gitlab-ci runner in your PC, registered to your project
3. Write a .gitlab-ci.yml to redeploy the phoenix service upon code push

## Stage 3 - Kubernetes

Todo

## Stage 4 - Istio

Todo

## Clean up

1. Uninstall the gitlab-ci runner
2. Uninstall docker and docker-compose
3. Commit all code to your project
4. Delete all code from the PC
